package com.culc.api;

import com.culc.service.CulcService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RestApi {

    private CulcService culcService;

    public void setCulcService(CulcService culcService) {
        this.culcService = culcService;
    }

    @GetMapping(value = "/add/{a}/{b}/", produces = MediaType.TEXT_HTML_VALUE)
    public String add (@PathVariable("a") double a, @PathVariable("b") double b ){
        return "<div> Result of add </div> <br> <div> " + culcService.add(a,b) + " </div>";
    }
    @GetMapping(value = "/deduct/{a}/{b}/", produces = MediaType.TEXT_HTML_VALUE)
    public String deduct (@PathVariable("a") double a, @PathVariable("b") double b ){
        return "<div> Result of add </div> <br> <div> " + culcService.deduct(a,b) + " </div>";
    }
    @GetMapping(value = "/multiply/{a}/{b}/", produces = MediaType.TEXT_HTML_VALUE)
    public String multiply(@PathVariable("a") double a, @PathVariable("b") double b ){
        return "<div> Result of add </div> <br> <div> " + culcService.multiply(a,b) + " </div>";
    }
    @GetMapping(value = "/divide/{a}/{b}/", produces = MediaType.TEXT_HTML_VALUE)
    public String divide(@PathVariable("a") double a, @PathVariable("b") double b ){
        if (b == 0.0){
            return "<div> Result of add </div> <br> <div> INF </div>";
        }else {
            return "<div> Result of add </div> <br> <div> " + culcService.divide(a,b) + " </div>";
        }

    }

}
