package com.culc.service;

public class CulcService {

    public double add (double a, double b){
        return a + b;
    }

    public double deduct (double a, double b){
        return a - b;
    }

    public double multiply (double a, double b){
        return a * b;
    }

    public double divide (double a, double b){
        return a / b;
    }
}
